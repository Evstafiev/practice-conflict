export default class Person {
    constructor(name) {
        this.name = name;
     
    }
    met(name) {
    	return `${this.name} dont expect to meet his friend ${name} here`
    }
    say(phrase) {
        return `${this.name} says ${phrase}`;
    }
}

